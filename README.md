# calculadora

package calculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class prueba {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton btnSumar;
	private JButton btnRestar;
	private JButton btnMultiplicar;
	private JButton btnDividir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					prueba window = new prueba();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public prueba() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 255, 102));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPrimero = new JLabel("Primero digito");
		lblPrimero.setForeground(Color.BLUE);
		lblPrimero.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
		lblPrimero.setBounds(10, 11, 128, 20);
		frame.getContentPane().add(lblPrimero);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.RIGHT);
		textField.setBounds(148, 11, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblSegundoDigito = new JLabel("Segundo digito");
		lblSegundoDigito.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
		lblSegundoDigito.setForeground(Color.BLUE);
		lblSegundoDigito.setBounds(10, 42, 128, 20);
		frame.getContentPane().add(lblSegundoDigito);
		
		textField_1 = new JTextField();
		textField_1.setHorizontalAlignment(SwingConstants.RIGHT);
		textField_1.setBounds(148, 42, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblTotal = new JLabel("Total");
		lblTotal.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
		lblTotal.setForeground(Color.RED);
		lblTotal.setBounds(70, 107, 68, 20);
		frame.getContentPane().add(lblTotal);
		
		textField_2 = new JTextField();
		textField_2.setBackground(Color.WHITE);
		textField_2.setEditable(false);
		textField_2.setHorizontalAlignment(SwingConstants.RIGHT);
		textField_2.setBounds(148, 108, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		btnSumar = new JButton("Sumar");
		btnSumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String resultado = Integer.toString(Integer.parseInt(textField.getText()) + Integer.parseInt(textField_1.getText()));
				textField_2.setText(resultado);
					
			}
		});
		btnSumar.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
		btnSumar.setForeground(Color.RED);
		btnSumar.setBounds(304, 105, 128, 20);
		frame.getContentPane().add(btnSumar);
		
		btnRestar = new JButton("Restar");
		btnRestar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String resultado = Integer.toString(Integer.parseInt(textField.getText()) - Integer.parseInt(textField_1.getText()));
				textField_2.setText(resultado);
			}
		});
		btnRestar.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
		btnRestar.setForeground(Color.RED);
		btnRestar.setBounds(304, 139, 128, 20);
		frame.getContentPane().add(btnRestar);
		
		btnMultiplicar = new JButton("Multiplicar");
		btnMultiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String resultado = Integer.toString(Integer.parseInt(textField.getText()) * Integer.parseInt(textField_1.getText()));
				textField_2.setText(resultado);
			}
		});
		btnMultiplicar.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
		btnMultiplicar.setForeground(Color.RED);
		btnMultiplicar.setBounds(304, 173, 128, 20);
		frame.getContentPane().add(btnMultiplicar);
		
		btnDividir = new JButton("Dividir");
		btnDividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String resultado = Integer.toString(Integer.parseInt(textField.getText()) / Integer.parseInt(textField_1.getText()));
				textField_2.setText(resultado);
			}
		});
		btnDividir.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
		btnDividir.setForeground(Color.RED);
		btnDividir.setBounds(304, 204, 128, 20);
		frame.getContentPane().add(btnDividir);
	}
}

prototipo